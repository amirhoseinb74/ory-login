package com.example.orylogin

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OryLoginApp : Application()