package com.example.orylogin.presentation.common

import com.example.orylogin.domain.model.LoginResponse

data class LoginState(
    val loading: Boolean = false,
    val data: LoginResponse? = null,
    val error: String? = null
)
