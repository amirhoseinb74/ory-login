package com.example.orylogin.presentation.login

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.navigation.NavController
import com.example.orylogin.presentation.sign_in.SignInScreen
import com.example.orylogin.presentation.sign_up.SignUpScreen

@Composable
fun LoginScreen(navController: NavController) {
    var hasAccount by remember {
        mutableStateOf(false)
    }

    if (hasAccount) {
        SignInScreen(
            navController = navController,
            onClickSignUp = {
                hasAccount = false
            })
    } else {
        SignUpScreen(
            navController = navController,
            onClickSignIn = {
                hasAccount = true
            })
    }
}