package com.example.orylogin.presentation.common.util

import androidx.navigation.NavController
import com.example.orylogin.domain.model.LoginResponse
import com.google.gson.Gson

fun NavController.navigateToLoginResult(
    loginResponse: LoginResponse
) {
    val jsonData: String = Gson().toJson(loginResponse)

    navigate(
        route = Screen.LoginResultScreen.route +
                "?${Screen.ARG_LOGIN_DATA}={$jsonData}"
    )
}