package com.example.orylogin.presentation.login_result

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.orylogin.domain.model.LoginResponse

@Composable
fun LoginResultScreen(loginResponse: LoginResponse) {

    Column(
        Modifier
            .fillMaxSize()
            .padding(24.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "You successfully signed in",
            style = MaterialTheme.typography.headlineSmall
        )
        Spacer(modifier = Modifier.height(16.dp))

        loginResponse.traits.run {
            TextWithLabel(text = username, label = "username")
            TextWithLabel(text = email, label = "email")
            TextWithLabel(text = fullName, label = "full name")
        }
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = "Expiration Date: ${loginResponse.expiresAt}",
            style = MaterialTheme.typography.bodyMedium
        )
    }
}

@Composable
private fun TextWithLabel(text: String?, label: String) {
    text?.let {
        Text(
            text = "$label: $it",
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 8.dp),
            textAlign = TextAlign.Start,
            style = MaterialTheme.typography.bodyLarge
        )
        Spacer(modifier = Modifier.height(5.dp))
    }
}
