package com.example.orylogin.presentation.common

sealed class LoginUIEvent {
    object LoginSuccess : LoginUIEvent()
}