package com.example.orylogin.presentation.sign_in

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.orylogin.R
import com.example.orylogin.presentation.common.LoginState
import com.example.orylogin.presentation.common.LoginUIEvent
import com.example.orylogin.presentation.common.components.AnnotatedClickableText
import com.example.orylogin.presentation.common.components.LoadingAnimation
import com.example.orylogin.presentation.common.ui.theme.OryLoginTheme
import com.example.orylogin.presentation.common.util.navigateToLoginResult
import kotlinx.coroutines.flow.collectLatest

@Composable
fun SignInScreen(
    navController: NavController,
    viewModel: SignInViewModel = hiltViewModel(),
    onClickSignUp: () -> Unit
) {
    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                LoginUIEvent.LoginSuccess -> {
                    viewModel.loginState.value.data?.let { navController.navigateToLoginResult(it) }
                }
            }
        }
    }

    SignInContent(
        sendEventToVm = viewModel::onEvent,
        identifier = viewModel.identifier.value,
        password = viewModel.password.value,
        loginState = viewModel.loginState.value,
        signInFieldsState = viewModel.signInFieldsState.value,
        onClickSignUp = onClickSignUp
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun SignInContent(
    sendEventToVm: (SignInEvent) -> Unit,
    identifier: String,
    password: String,
    loginState: LoginState,
    signInFieldsState: SignInFieldsState,
    onClickSignUp: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        OutlinedTextField(
            value = identifier,
            singleLine = true,
            onValueChange = { sendEventToVm(SignInEvent.EnteredIdentifier(it)) },
            label = { Text(signInFieldsState.data) },
            modifier = Modifier.fillMaxWidth()
        )
        signInFieldsState.error?.takeIf { it.isNotBlank() }?.let { error ->
            Text(text = error, color = Color.Red)
            Spacer(modifier = Modifier.height(8.dp))
        }
        Spacer(modifier = Modifier.height(8.dp))
        OutlinedTextField(
            value = password,
            singleLine = true,
            onValueChange = { sendEventToVm(SignInEvent.EnteredPassword(it)) },
            label = { Text(stringResource(R.string.password)) },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password,
                imeAction = ImeAction.Done
            ),
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier.fillMaxWidth(),
            keyboardActions = KeyboardActions(onDone = { sendEventToVm(SignInEvent.SignInClicked) })
        )
        Spacer(modifier = Modifier.height(8.dp))
        loginState.error?.takeIf { it.isNotBlank() }?.let { error ->
            Text(text = error, color = Color.Red)
            Spacer(modifier = Modifier.height(8.dp))
        }
        Button(
            onClick = { sendEventToVm(SignInEvent.SignInClicked) },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(stringResource(R.string.sign_in))
        }
        Spacer(modifier = Modifier.height(12.dp))
        AnnotatedClickableText(hasAccount = true, onClick = onClickSignUp)
    }
    if (loginState.loading || signInFieldsState.loading) {
        LoadingAnimation()
    }
}

@Preview
@Composable
fun SignInScreenPreview() {
    OryLoginTheme {
        SignInContent(
            sendEventToVm = {},
            identifier = "",
            password = "",
            loginState = LoginState(),
            signInFieldsState = SignInFieldsState(data = "E-Mail"),
            onClickSignUp = {}
        )
    }
}