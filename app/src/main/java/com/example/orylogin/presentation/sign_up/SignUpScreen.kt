package com.example.orylogin.presentation.sign_up

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.orylogin.R
import com.example.orylogin.presentation.common.LoginState
import com.example.orylogin.presentation.common.LoginUIEvent
import com.example.orylogin.presentation.common.components.AnnotatedClickableText
import com.example.orylogin.presentation.common.components.LoadingAnimation
import com.example.orylogin.presentation.common.ui.theme.OryLoginTheme
import com.example.orylogin.presentation.common.util.navigateToLoginResult
import com.example.orylogin.presentation.sign_up.components.DynamicSignUpForm
import kotlinx.coroutines.flow.collectLatest

@Composable
fun SignUpScreen(
    navController: NavController,
    viewModel: SignUpViewModel = hiltViewModel(),
    onClickSignIn: () -> Unit
) {
    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when (event) {
                LoginUIEvent.LoginSuccess -> {
                    viewModel.loginState.value.data?.let { navController.navigateToLoginResult(it) }
                }
            }
        }
    }

    SignUpContent(
        sendEventToVM = viewModel::onEvent,
        loginState = viewModel.loginState.value,
        signUpFieldsState = viewModel.signUpFieldsState.value,
        onClickSignIn = onClickSignIn
    )
}

@Composable
private fun SignUpContent(
    sendEventToVM: (SignUpEvent) -> Unit,
    loginState: LoginState,
    signUpFieldsState: SignUpFieldsState,
    onClickSignIn: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        DynamicSignUpForm(nodes = signUpFieldsState.data, sendEventToVM = sendEventToVM)
        signUpFieldsState.error?.takeIf { it.isNotBlank() }?.let { error ->
            Text(text = error, color = Color.Red)
            Spacer(modifier = Modifier.height(8.dp))
        }
        loginState.error?.takeIf { it.isNotBlank() }?.let { error ->
            Text(text = error, color = Color.Red)
            Spacer(modifier = Modifier.height(8.dp))
        }
        Button(
            onClick = { sendEventToVM(SignUpEvent.SignUpClicked) },
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(stringResource(R.string.sign_up))
        }
        Spacer(modifier = Modifier.height(12.dp))
        AnnotatedClickableText(hasAccount = false, onClick = onClickSignIn)
    }
    if (loginState.loading || signUpFieldsState.loading) {
        LoadingAnimation()
    }
}

@Preview
@Composable
fun SignUpScreenPreview() {
    OryLoginTheme {
        SignUpContent(
            sendEventToVM = {},
            loginState = LoginState(),
            signUpFieldsState = SignUpFieldsState(),
            onClickSignIn = {}
        )
    }
}