package com.example.orylogin.presentation.common.components

import androidx.compose.foundation.text.ClickableText
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.sp
import com.example.orylogin.R

@Composable
fun AnnotatedClickableText(hasAccount: Boolean, onClick: () -> Unit) {
    val questionText =
        if (hasAccount)
            stringResource(id = R.string.don_t_have_an_account)
        else
            stringResource(id = R.string.already_have_an_account)

    val clickableText =
        if (hasAccount)
            stringResource(id = R.string.sign_up)
        else
            stringResource(id = R.string.sign_in)

    val annotatedString = buildAnnotatedString {
        withStyle(
            style = SpanStyle(
                color = Color.Gray,
                fontSize = 16.sp
            )
        ) {
            append(questionText)
        }
        withStyle(
            style = SpanStyle(
                color = Color.Blue,
                fontSize = 16.sp,
                textDecoration = TextDecoration.Underline
            )
        ) {
            pushStringAnnotation(tag = clickableText, annotation = clickableText)
            append(clickableText)
        }
    }
    ClickableText(text = annotatedString, onClick = { onClick() })
}