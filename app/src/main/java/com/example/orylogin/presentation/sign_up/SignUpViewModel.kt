package com.example.orylogin.presentation.sign_up

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.orylogin.data.remote.dto.response.Result
import com.example.orylogin.domain.use_case.GetSignUpInputFields
import com.example.orylogin.domain.use_case.SignUpUseCase
import com.example.orylogin.presentation.common.LoginState
import com.example.orylogin.presentation.common.LoginUIEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val signUpUseCase: SignUpUseCase,
    private val getSignUpInputFields: GetSignUpInputFields
) : ViewModel() {
    private var email = ""
    private var password = ""
    private var fullName = ""
    private var username = ""

    private val _loginState = mutableStateOf(LoginState())
    val loginState: State<LoginState> = _loginState

    private val _signUpFieldsState = mutableStateOf(SignUpFieldsState())
    val signUpFieldsState: State<SignUpFieldsState> = _signUpFieldsState

    private val _eventFlow = MutableSharedFlow<LoginUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        getSignUpFields()
    }

    fun onEvent(event: SignUpEvent) {
        when (event) {
            is SignUpEvent.EnteredValue -> {
                when (event.name) {
                    "traits.email" -> email = event.value
                    "password" -> password = event.value
                    "traits.fullName" -> fullName = event.value
                    "traits.username" -> username = event.value
                }
            }

            SignUpEvent.SignUpClicked -> {
                signUp()
            }
        }
    }

    private fun getSignUpFields() {
        getSignUpInputFields().onEach { result ->
            _signUpFieldsState.value = when (result) {
                is Result.Loading -> {
                    SignUpFieldsState(loading = true)
                }

                is Result.Success -> {
                    result.data?.let { SignUpFieldsState(data = it) } ?: SignUpFieldsState()
                }

                is Result.Error -> {
                    SignUpFieldsState(error = result.message)
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun signUp() {
        signUpUseCase(
            email = email,
            password = password,
            fullName = fullName,
            username = username
        ).onEach { result ->
            _loginState.value = when (result) {
                is Result.Loading -> {
                    LoginState(loading = true)
                }

                is Result.Success -> {
                    _eventFlow.emit(LoginUIEvent.LoginSuccess)
                    LoginState(data = result.data)
                }

                is Result.Error -> {
                    LoginState(error = result.message)
                }
            }
        }.launchIn(viewModelScope)
    }
}