package com.example.orylogin.presentation.sign_up

sealed class SignUpEvent {
    data class EnteredValue(val name: String, val value: String) : SignUpEvent()
    object SignUpClicked : SignUpEvent()
}