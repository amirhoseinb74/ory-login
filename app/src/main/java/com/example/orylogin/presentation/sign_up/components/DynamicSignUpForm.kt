package com.example.orylogin.presentation.sign_up.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.example.orylogin.data.remote.dto.response.flow.Node
import com.example.orylogin.data.remote.dto.response.flow.isValidForInput
import com.example.orylogin.presentation.sign_up.SignUpEvent

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DynamicSignUpForm(nodes: List<Node>, sendEventToVM: (SignUpEvent) -> Unit) {
    Column {
        nodes.filter { it.isValidForInput() }.forEach { node ->
            val name = node.attributes?.name ?: ""
            val label = node.meta?.label?.text ?: ""
            var fieldValue by remember { mutableStateOf("") }

            OutlinedTextField(
                value = fieldValue,
                onValueChange = {
                    fieldValue = it
                    sendEventToVM(SignUpEvent.EnteredValue(name, it))
                },
                label = { Text(text = label) },
                singleLine = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 8.dp),
                keyboardOptions = KeyboardOptions.Default.copy(
                    keyboardType = when (node.attributes?.type) {
                        "email" -> KeyboardType.Email
                        "password" -> KeyboardType.Password
                        else -> KeyboardType.Text
                    },
                    imeAction = ImeAction.Done
                ),
                visualTransformation =
                if (node.attributes?.type == "password") PasswordVisualTransformation()
                else VisualTransformation.None
            )
        }
    }
}