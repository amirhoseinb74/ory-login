package com.example.orylogin.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.orylogin.domain.model.LoginResponse
import com.example.orylogin.presentation.common.ui.theme.OryLoginTheme
import com.example.orylogin.presentation.common.util.Screen
import com.example.orylogin.presentation.login.LoginScreen
import com.example.orylogin.presentation.login_result.LoginResultScreen
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            OryLoginTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val navController = rememberNavController()
                    NavHost(
                        navController = navController,
                        startDestination = Screen.LoginScreen.route
                    ) {
                        composable(Screen.LoginScreen.route) {
                            LoginScreen(navController)
                        }
                        composable(
                            route = Screen.LoginResultScreen.route +
                                    "?${Screen.ARG_LOGIN_DATA}={${Screen.ARG_LOGIN_DATA}}"
                        ) {
                            it.arguments?.getString(Screen.ARG_LOGIN_DATA)?.let { loginResultJson ->
                                val loginResponse = Gson().fromJson(
                                    loginResultJson.substring(1, loginResultJson.length - 1),
                                    LoginResponse::class.java
                                )
                                LoginResultScreen(loginResponse)
                            }
                        }
                    }
                }
            }
        }
    }
}