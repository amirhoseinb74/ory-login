package com.example.orylogin.presentation.sign_in

data class SignInFieldsState(
    val loading: Boolean = false,
    val data: String = "",
    val error: String? = null
)