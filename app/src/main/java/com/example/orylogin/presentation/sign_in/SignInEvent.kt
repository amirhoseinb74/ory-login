package com.example.orylogin.presentation.sign_in

sealed class SignInEvent {
    data class EnteredIdentifier(val identifier: String) : SignInEvent()
    data class EnteredPassword(val password: String) : SignInEvent()
    object SignInClicked : SignInEvent()
}