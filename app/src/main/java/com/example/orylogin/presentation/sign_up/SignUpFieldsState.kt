package com.example.orylogin.presentation.sign_up

import com.example.orylogin.data.remote.dto.response.flow.Node

data class SignUpFieldsState(
    val loading: Boolean = false,
    val data: List<Node> = emptyList(),
    val error: String? = null
)
