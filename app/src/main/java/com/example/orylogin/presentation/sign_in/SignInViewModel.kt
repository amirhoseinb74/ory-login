package com.example.orylogin.presentation.sign_in

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.orylogin.data.remote.dto.response.Result
import com.example.orylogin.data.remote.dto.response.flow.Node
import com.example.orylogin.domain.use_case.GetSignInInputFields
import com.example.orylogin.domain.use_case.SignInUseCase
import com.example.orylogin.presentation.common.LoginState
import com.example.orylogin.presentation.common.LoginUIEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val signInUseCase: SignInUseCase,
    private val getSignInInputFields: GetSignInInputFields
) : ViewModel() {

    private val _identifier = mutableStateOf("")
    val identifier: State<String> = _identifier

//    private val _identifierLabel = mutableStateOf("")
//    val identifierLabel: State<String> = _identifierLabel

    private val _password = mutableStateOf("")
    val password: State<String> = _password

    private val _loginState = mutableStateOf(LoginState())
    val loginState: State<LoginState> = _loginState

    private val _signInFieldsState = mutableStateOf(SignInFieldsState())
    val signInFieldsState: State<SignInFieldsState> = _signInFieldsState

    private val _eventFlow = MutableSharedFlow<LoginUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        getSignInField()
    }

    fun onEvent(event: SignInEvent) {
        when (event) {
            is SignInEvent.EnteredIdentifier -> {
                _identifier.value = event.identifier
            }

            is SignInEvent.EnteredPassword -> {
                _password.value = event.password
            }

            SignInEvent.SignInClicked -> {
                signIn()
            }
        }
    }

    private fun getSignInField() {
        getSignInInputFields().onEach { result ->
            _signInFieldsState.value = when (result) {
                is Result.Loading -> {
                    SignInFieldsState(loading = true)
                }

                is Result.Success -> {
                    SignInFieldsState(data = getIdentifierLabel(result.data))
                }

                is Result.Error -> {
                    SignInFieldsState(error = result.message)
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun getIdentifierLabel(data: List<Node>?): String =
        data?.first { it.attributes?.name == "identifier" }?.meta?.label?.text.orEmpty()


    private fun signIn() {
        signInUseCase(
            email = identifier.value,
            password = password.value
        ).onEach { result ->
            _loginState.value = when (result) {
                is Result.Loading -> {
                    LoginState(loading = true)
                }

                is Result.Success -> {
                    _eventFlow.emit(LoginUIEvent.LoginSuccess)
                    LoginState(data = result.data)
                }

                is Result.Error -> {
                    LoginState(error = result.message)
                }
            }
        }.launchIn(viewModelScope)
    }
}