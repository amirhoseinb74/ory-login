package com.example.orylogin.presentation.common.util

sealed class Screen(val route: String) {
    object LoginScreen : Screen("screen_login")
    object LoginResultScreen : Screen("screen_login_result")

    companion object {
        const val ARG_LOGIN_DATA = "ARG_LOGIN_DATA"
    }
}