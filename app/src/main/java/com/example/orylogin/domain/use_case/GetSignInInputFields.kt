package com.example.orylogin.domain.use_case

import com.example.orylogin.data.remote.dto.response.Result
import com.example.orylogin.data.remote.dto.response.flow.Node
import com.example.orylogin.domain.repository.UserRepository
import com.example.orylogin.domain.util.HttpErrorParser
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject


class GetSignInInputFields @Inject constructor(
    private val repository: UserRepository
) {
    operator fun invoke(): Flow<Result<List<Node>>> = flow {
        try {
            emit(Result.Loading())
            val result = repository.getSignInFlow()
            result.ui?.nodes?.let { nodes ->
                emit(Result.Success(nodes))
            } ?: emit(Result.Error("Can't Find Input Nodes."))
        } catch (e: HttpException) {
            emit(Result.Error(HttpErrorParser.parseHttpException(e)))
        } catch (e: IOException) {
            emit(Result.Error("Couldn't reach server. Check your internet connection."))
        }
    }
}