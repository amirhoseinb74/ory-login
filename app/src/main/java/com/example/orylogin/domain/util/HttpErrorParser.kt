package com.example.orylogin.domain.util

import com.example.orylogin.data.remote.dto.response.flow.FlowResponse
import com.google.gson.Gson
import com.google.gson.JsonParseException
import retrofit2.HttpException

class HttpErrorParser {
    companion object {
        fun parseHttpException(e: HttpException): String {
            return when (e.code()) {
                in 400..499 -> {
                    val errorBody = e.response()?.errorBody()?.string()
                    extractErrorMessage(errorBody) ?: "An unexpected client error occurred"
                }

                else -> "An unexpected server error occurred"
            }
        }

        private fun extractErrorMessage(errorBody: String?): String? {
            return try {
                val flowResponse = Gson().fromJson(errorBody, FlowResponse::class.java)
                val nodes = flowResponse.ui?.nodes
                if (nodes != null) {
                    for (node in nodes) {
                        val messages = node.messages
                        if (!messages.isNullOrEmpty()) {
                            for (message in messages) {
                                if (message.type == "error") {
                                    return message.text
                                }
                            }
                        }
                    }
                }
                val messages = flowResponse.ui?.messages
                if (!messages.isNullOrEmpty()) {
                    for (message in messages) {
                        if (message.type == "error") {
                            return message.text
                        }
                    }
                }
                null
            } catch (e: JsonParseException) {
                null
            }
        }
    }
}

