package com.example.orylogin.domain.model

import com.example.orylogin.data.remote.dto.request.Traits

data class LoginResponse(
    val traits: Traits = Traits(),
    val expiresAt: String
)