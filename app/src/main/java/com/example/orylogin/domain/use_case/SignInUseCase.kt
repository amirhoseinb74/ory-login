package com.example.orylogin.domain.use_case

import com.example.orylogin.data.remote.dto.request.LoginRequest
import com.example.orylogin.data.remote.dto.response.Result
import com.example.orylogin.data.remote.dto.response.login.LoginResponseDto
import com.example.orylogin.domain.model.LoginResponse
import com.example.orylogin.domain.repository.UserRepository
import com.example.orylogin.domain.util.HttpErrorParser
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class SignInUseCase @Inject constructor(
    private val repository: UserRepository
) {
    operator fun invoke(
        email: String,
        password: String
    ): Flow<Result<LoginResponse>> = flow {
        try {
            emit(Result.Loading())
            val signInFlow = repository.getSignInFlow().id
            signInFlow?.let {
                val data: LoginResponse = getSignInResult(email, password, it).toLoginResponse()
                emit(Result.Success(data))
            } ?: emit(Result.Error("Error getting Flow"))
        } catch (e: HttpException) {
            emit(Result.Error(HttpErrorParser.parseHttpException(e)))
        } catch (e: IOException) {
            emit(Result.Error("Couldn't reach server. Check your internet connection."))
        }
    }

    private suspend fun getSignInResult(
        email: String,
        password: String,
        flow: String
    ): LoginResponseDto {
        val loginRequest = LoginRequest(
            csrf_token = "",
            method = "password",
            password = password,
            identifier = email,
        )
        return repository.signIn(loginRequest, flow)
    }
}
