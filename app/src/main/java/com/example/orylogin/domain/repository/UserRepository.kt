package com.example.orylogin.domain.repository

import com.example.orylogin.data.remote.dto.request.LoginRequest
import com.example.orylogin.data.remote.dto.response.flow.FlowResponse
import com.example.orylogin.data.remote.dto.response.login.LoginResponseDto

interface UserRepository {

    suspend fun getSignUpFlow(): FlowResponse

    suspend fun registerNewUser(loginRequest: LoginRequest, flow: String): LoginResponseDto

    suspend fun getSignInFlow(): FlowResponse

    suspend fun signIn(loginRequest: LoginRequest, flow: String): LoginResponseDto
}