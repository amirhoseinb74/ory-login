package com.example.orylogin.domain.use_case

import com.example.orylogin.data.remote.dto.request.LoginRequest
import com.example.orylogin.data.remote.dto.request.Traits
import com.example.orylogin.data.remote.dto.response.Result
import com.example.orylogin.domain.model.LoginResponse
import com.example.orylogin.domain.repository.UserRepository
import com.example.orylogin.domain.util.HttpErrorParser
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class SignUpUseCase @Inject constructor(
    private val repository: UserRepository
) {
    operator fun invoke(
        email: String,
        password: String,
        fullName: String,
        username: String
    ): Flow<Result<LoginResponse>> = flow {
        try {
            emit(Result.Loading())
            val signUpFlow = repository.getSignUpFlow().id
            signUpFlow?.let {
                val data: LoginResponse = getSignUpResult(
                    email = email,
                    fullName = fullName,
                    username = username,
                    password = password,
                    flow = signUpFlow
                )
                emit(Result.Success(data))
            } ?: emit(Result.Error("Error getting Flow"))
        } catch (e: HttpException) {
            emit(Result.Error(HttpErrorParser.parseHttpException(e)))
        } catch (e: IOException) {
            emit(Result.Error("Couldn't reach server. Check your internet connection."))
        }
    }

    private suspend fun getSignUpResult(
        email: String,
        fullName: String,
        username: String,
        password: String,
        flow: String
    ): LoginResponse {

        val traits = Traits(
            email = email.ifBlank { null },
            fullName = fullName.ifBlank { null },
            username = username.ifBlank { null },
        )

        val loginRequest = LoginRequest(
            csrf_token = "",
            method = "password",
            password = password,
            traits = traits
        )
        return repository.registerNewUser(loginRequest, flow).toLoginResponse()
    }
}
