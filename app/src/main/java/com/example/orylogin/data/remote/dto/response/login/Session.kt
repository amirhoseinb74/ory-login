package com.example.orylogin.data.remote.dto.response.login

import com.google.gson.annotations.SerializedName

data class Session(
    val active: Boolean,
    @SerializedName("authenticated_at") val authenticatedAt: String,
    @SerializedName("authentication_methods") val authenticationMethods: List<AuthenticationMethod>,
    @SerializedName("authenticator_assurance_level") val authenticatorAssuranceLevel: String,
    val devices: List<Device>,
    @SerializedName("expires_at") val expiresAt: String,
    val id: String,
    val identity: Identity,
    @SerializedName("issued_at") val issuedAt: String
)
