package com.example.orylogin.data.remote.dto.request

data class Traits(
    val email: String? = null,
    val fullName: String? = null,
    val username: String? = null
)