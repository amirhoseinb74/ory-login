package com.example.orylogin.data.remote.dto.response.flow

data class Message(
    val id: Int,
    val text: String,
    val type: String
)