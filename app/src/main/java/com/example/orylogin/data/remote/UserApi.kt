package com.example.orylogin.data.remote

import com.example.orylogin.data.remote.dto.request.LoginRequest
import com.example.orylogin.data.remote.dto.response.flow.FlowResponse
import com.example.orylogin.data.remote.dto.response.login.LoginResponseDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface UserApi {

    @GET("self-service/registration/api")
    suspend fun getRegistrationFlow(): FlowResponse

    @POST("self-service/registration")
    suspend fun registerNewUser(
        @Body loginRequest: LoginRequest,
        @Query("flow") flow: String
    ): LoginResponseDto

    @GET("self-service/login/api")
    suspend fun getSignInFlow(): FlowResponse

    @POST("self-service/login")
    suspend fun signIn(
        @Body loginRequest: LoginRequest,
        @Query("flow") flow: String
    ): LoginResponseDto
}