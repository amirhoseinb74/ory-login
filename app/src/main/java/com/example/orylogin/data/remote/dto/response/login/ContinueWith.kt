package com.example.orylogin.data.remote.dto.response.login

import com.google.gson.annotations.SerializedName

data class ContinueWith(
    val action: String,
    val flow: Flow,
    @SerializedName("ory_session_token") val orySessionToken: String
)