package com.example.orylogin.data.remote.dto.response.flow

data class Ui(
    val action: String?,
    val messages: List<Message>?,
    val method: String?,
    val nodes: List<Node>?
)