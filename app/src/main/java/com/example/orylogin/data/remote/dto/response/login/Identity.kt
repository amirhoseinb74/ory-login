package com.example.orylogin.data.remote.dto.response.login

import com.example.orylogin.data.remote.dto.request.Traits
import com.google.gson.annotations.SerializedName

data class Identity(
    @SerializedName("created_at") val createdAt: String,
    val id: String,
    @SerializedName("metadata_public") val metadataPublic: Any,
    @SerializedName("organization_id") val organizationId: Any,
    @SerializedName("recovery_addresses") val recoveryAddresses: List<RecoveryAddress>,
    @SerializedName("schema_id") val schemaId: String,
    @SerializedName("schema_url") val schemaUrl: String,
    val state: String,
    @SerializedName("state_changed_at") val stateChangedAt: String,
    val traits: Traits,
    @SerializedName("updated_at") val updatedAt: String,
    @SerializedName("verifiable_addresses") val verifiableAddresses: List<VerifiableAddress>
)
