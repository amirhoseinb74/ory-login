package com.example.orylogin.data.remote.dto.response.login

import com.google.gson.annotations.SerializedName

data class VerifiableAddress(
    @SerializedName("created_at") val createdAt: String,
    val id: String,
    val status: String,
    @SerializedName("updated_at") val updatedAt: String,
    val value: String,
    val verified: Boolean,
    val via: String
)