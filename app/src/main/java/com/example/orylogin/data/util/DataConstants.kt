package com.example.orylogin.data.util

object DataConstants {
    const val BASE_URL = "https://recursing-rubin-qtfykhiqyl.projects.oryapis.com/"
    const val NETWORK_TIMEOUT = 60L //in seconds
}