package com.example.orylogin.data.remote.dto.response.flow

data class Node(
    val attributes: Attributes?,
    val group: String?,
    val messages: List<Message>?,
    val meta: Meta?,
    val type: String?
)

fun Node.isValidForInput(): Boolean {
    return type == "input" && attributes?.let { it.type != "hidden" && it.type != "submit" } ?: false
}