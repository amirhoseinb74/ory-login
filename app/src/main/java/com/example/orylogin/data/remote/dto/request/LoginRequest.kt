package com.example.orylogin.data.remote.dto.request

data class LoginRequest(
    val csrf_token: String?,
    val method: String?,
    val password: String?,
    val traits: Traits? = null,
    val identifier: String? = null,
)