package com.example.orylogin.data.remote.dto.response.flow

data class Meta(
    val label: Label?
)