package com.example.orylogin.data.remote.dto.response.login

import com.google.gson.annotations.SerializedName

data class RecoveryAddress(
    @SerializedName("created_at") val createdAt: String,
    val id: String,
    @SerializedName("updated_at") val updatedAt: String,
    val value: String,
    val via: String
)