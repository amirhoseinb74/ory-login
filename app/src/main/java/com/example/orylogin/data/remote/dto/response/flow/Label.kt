package com.example.orylogin.data.remote.dto.response.flow

data class Label(
    val context: Context,
    val id: Int,
    val text: String,
    val type: String
)