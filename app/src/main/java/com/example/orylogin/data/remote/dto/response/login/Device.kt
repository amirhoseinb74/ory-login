package com.example.orylogin.data.remote.dto.response.login

import com.google.gson.annotations.SerializedName

data class Device(
    val id: String,
    @SerializedName("ip_address") val ipAddress: String,
    val location: String,
    @SerializedName("user_agent") val userAgent: String
)