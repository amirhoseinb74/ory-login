package com.example.orylogin.data.remote.dto.response.flow

import com.google.gson.annotations.SerializedName

data class Attributes(
    val autocomplete: String,
    val disabled: Boolean,
    val name: String,
    @SerializedName("node_type") val nodeType: String,
    val required: Boolean,
    val type: String,
    val value: String
)