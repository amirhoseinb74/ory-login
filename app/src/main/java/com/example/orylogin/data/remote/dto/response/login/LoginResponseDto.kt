package com.example.orylogin.data.remote.dto.response.login

import com.example.orylogin.data.remote.dto.request.Traits
import com.example.orylogin.domain.model.LoginResponse
import com.google.gson.annotations.SerializedName

data class LoginResponseDto(
    @SerializedName("continue_with") val continueWith: List<ContinueWith>?,
    val identity: Identity?,
    val session: Session?,
    @SerializedName("session_token") val sessionToken: String?
) {
    fun toLoginResponse(): LoginResponse =
        LoginResponse(
            traits = identity?.traits ?: Traits(),
            expiresAt = session?.expiresAt ?: ""
        )
}