package com.example.orylogin.data.remote.dto.response.login

import com.google.gson.annotations.SerializedName

data class Flow(
    val id: String,
    @SerializedName("verifiable_address") val verifiableAddress: String
)