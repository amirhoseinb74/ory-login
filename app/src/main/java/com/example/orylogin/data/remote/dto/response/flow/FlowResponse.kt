package com.example.orylogin.data.remote.dto.response.flow

import com.google.gson.annotations.SerializedName

data class FlowResponse(
    @SerializedName("created_at") val createdAt: String?,
    @SerializedName("expires_at") val expiresAt: String?,
    val id: String?,
    @SerializedName("issued_at") val issuedAt: String?,
    @SerializedName("organization_id") val organizationId: Any?,
    val refresh: Boolean?,
    @SerializedName("request_url") val requestUrl: String?,
    @SerializedName("requested_aal") val requestedAal: String?,
    val state: String?,
    val type: String?,
    val ui: Ui?,
    @SerializedName("updated_at") val updatedAt: String?
)
