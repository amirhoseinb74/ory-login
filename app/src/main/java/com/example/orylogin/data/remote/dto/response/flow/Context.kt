package com.example.orylogin.data.remote.dto.response.flow

data class Context(
    val title: String
)