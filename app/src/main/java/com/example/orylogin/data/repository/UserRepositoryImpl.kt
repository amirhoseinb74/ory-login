package com.example.orylogin.data.repository

import com.example.orylogin.data.remote.UserApi
import com.example.orylogin.data.remote.dto.request.LoginRequest
import com.example.orylogin.data.remote.dto.response.flow.FlowResponse
import com.example.orylogin.data.remote.dto.response.login.LoginResponseDto
import com.example.orylogin.domain.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val api: UserApi
) : UserRepository {

    override suspend fun getSignUpFlow(): FlowResponse {
        return api.getRegistrationFlow()
    }

    override suspend fun registerNewUser(
        loginRequest: LoginRequest,
        flow: String
    ): LoginResponseDto {
        return api.registerNewUser(loginRequest, flow)
    }

    override suspend fun getSignInFlow(): FlowResponse {
        return api.getSignInFlow()
    }

    override suspend fun signIn(loginRequest: LoginRequest, flow: String): LoginResponseDto {
        return api.signIn(loginRequest, flow)
    }
}