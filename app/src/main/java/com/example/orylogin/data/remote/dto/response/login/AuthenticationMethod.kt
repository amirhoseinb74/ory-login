package com.example.orylogin.data.remote.dto.response.login

import com.google.gson.annotations.SerializedName

data class AuthenticationMethod(
    val aal: String,
    @SerializedName("completed_at") val completedAt: String,
    val method: String
)